#include "constructs.h"
#include <gluplot.h>
using namespace glp;

#define CIMG  0


#define STDP 0

#if  CIMG
#include "CImg.h"
using namespace cimg_library;
#endif


MTRand rgen(1980); 


inline float branch_nonlinearity(float V)
{
	return 1./(1.+exp((3.6-V)/2.0)) + 0.3*V + 0.0144*V*V;
}


inline float calcium_step(float post_depolarization)
{
	return 0.1/(1.+exp( (-(post_depolarization+30.0))/9.0));
}


inline float caDP(float ca)
{
	return (1.5/(1.+exp(-(ca*10.-5.)*10.))) - (0.5/(1+exp(-(ca*10.0-3.5)*6.0)));
}


void LANetwork::CreateNeurons(int number, char type, vector<LANeuron*>* appendTo = 0)
{
	for (int i =0 ;i < number; i++)
	{
		LANeuron* n ;
		if (type == 'S')
			n = new LAInput();
		else
		{
			n = new LANeuron();

			for (int tt =0; tt < N_BRANCHES_PER_NEURON; tt++)
			{
				LABranch* bb  = new LABranch;
				bb->bid = this->branches.size();
				this->branches.push_back(bb);
				n->branches.push_back(bb);
			}
		}
		n->type = type;
		n->V = param_V_rest +1.0;
		n->nid = this->neurons.size();

		n->glx = 1.9*(n->nid%50)/50. - 0.9;
		n->gly = 1.9*(n->nid/50)/50. - 0.9;

		this->neurons.push_back(n);
		if (appendTo)
			appendTo->push_back(n);
	}
}



void LANetwork::CalculateDistances()
{
	for (nrn_iter na = neurons.begin(); na != neurons.end(); ++na)
		for (nrn_iter nb = neurons.begin(); nb != neurons.end(); ++nb)
			if (nb != na)
			{
				LANeuron* a = *na, *b=*nb;
				
				float dx = b->pos_x - a->pos_x;
				float dy = b->pos_y - a->pos_y;
				float dz = b->pos_z - a->pos_z;
				distances[pair<int,int>(a->nid,b->nid)] = (double)sqrt(dx*dx + dy*dy + dz*dz);
			}
}


int LANetwork::ConnectByDistance(vector<LANeuron*> fromList, vector<LANeuron*> toList, float fromDistance, float toDistance, int nNeuronPairs, int nSynapsesPerNeuron)
{
	int tpairs =0;
	while(true)
	{
		LANeuron* a = fromList.at(int(rgen()*(float)fromList.size()));
		LANeuron* b = toList.at(int(rgen()*(float)toList.size()));

		if (distances[pair<int,int>(a->nid, b->nid)] >= fromDistance && distances[pair<int,int>(a->nid, b->nid)] <= toDistance) 
		{
			for (int i =0; i < nSynapsesPerNeuron; i++)
			{
				LASynapse* syn = new LASynapse();
				syn->sid  = this->synapses.size();
				syn->source_nrn = a;
				syn->target_nrn = b;
				syn->target_branch = syn->target_nrn->branches[rgen()*float(syn->target_nrn->branches.size())];

				syn->source_nrn->outgoing.push_back(syn);
				syn->target_nrn->incoming.push_back(syn);
				syn->target_branch->synapses.push_back(syn);

				syn->pos = rgen();
				this->synapses.push_back(syn);
			}
		}

		if (++tpairs >= nNeuronPairs) break;
	}
	return tpairs;
}


void LANetwork::Create()
{
	int n_neurons = 200;
	this->nNeurons = n_neurons;

	CreateNeurons(n_neurons*0.8, 'P', &this->pyr_list);
	CreateNeurons(n_neurons*0.2, 'I', &this->in_list);
	

	int n_inputs = 4;
	int n_per_input = 10;
	for (int i=0;i < n_inputs;i++)
	{
		vector<LANeuron*> nrnsCS, nrnsUS;

		CreateNeurons(n_per_input, 'S', &nrnsCS);
		CreateNeurons(n_per_input, 'S', &nrnsUS);


		this->inputs_cs.push_back(nrnsCS);
		this->inputs_us.push_back(nrnsUS);

	}

	this->CalculateDistances();

	ConnectByDistance(this->pyr_list, this->pyr_list, 0.5, 1.5, 0.1*float(n_neurons)*0.5, 10);
	ConnectByDistance(this->pyr_list, this->in_list, 0.1, 0.5, 0.1*float(n_neurons)*0.5, 10);
	ConnectByDistance(this->in_list, this->pyr_list, 0.1, 0.5, 0.1*float(n_neurons)*0.5, 10);


	for (uint i =0; i < this->inputs_cs.size(); i++)
	{
		ConnectByDistance(this->inputs_cs[i], this->pyr_list, 0.0, 3.0, 1.7*float(this->pyr_list.size()), 10);
		ConnectByDistance(this->inputs_cs[i], this->in_list, 0.0, 2.0, 0.4*float(this->in_list.size()), 10);

		ConnectByDistance(this->inputs_us[i], this->pyr_list, 0.0, 3.0, 1.7*float(this->pyr_list.size()), 10);
		ConnectByDistance(this->inputs_us[i], this->in_list, 0.0, 2.0, 0.4*float(this->in_list.size()), 10);
	}


}







void LANetwork::StimDynamics(int duration) // stimulation dynamics, with dt=1msec 
{
	int t = 0;
	int spikeState[this->neurons.size()+1];
	int totalSpikes[this->neurons.size()+1];
	int lastSpikeT[this->neurons.size()+1];
	fill_n(spikeState, this->neurons.size()+1, 0);
	fill_n(totalSpikes, this->neurons.size()+1, 0);
	fill_n(lastSpikeT, this->neurons.size()+1, 0);

	this->voltageData = new Arr2D(duration, this->neurons.size());


	//gluplot gplot;

#if CIMG
	CImg<float> vimg(duration, this->neurons.size());
	CImg<float> synimg(duration, this->neurons.size());
	vimg.fill(0);
	synimg.fill(0);
#endif

	FILE* outfile = fopen("/tmp/wdata.dat", "w");
	FILE* spfile = fopen("/tmp/spdata.dat", "w");


	//curve vsurf;
	for (t=0; t < duration; t++)
	{
		for (syn_iter si=this->synapses.begin(); si != this->synapses.end(); ++si)
		{
			LASynapse* s = *si;
			LANeuron* src = s->source_nrn;


			//float stdp  =0.0;

			if (spikeState[src->nid])
			{
				s->target_branch->V += (0.0*s->eltp + s->weight)* ((src->type == 'I') ? -0.8 : 1.0); // XXX

#if STDP
				if (t > 30)
				{
					stdp -= exp(-(float(t - lastSpikeT[s->target_nrn->nid])) / 14.0);
				}
#endif


				s->calcium += (1.0 - calcium_step(s->target_branch->V));

			}

#if STDP

			if (spikeState[s->target_nrn->nid] && t > 30)
			{
				stdp += exp(-float(t-lastSpikeT[src->nid])/14.0);
			}

			stdp *= 0.01;

			if (stdp != 0.0 )
			{
				if (stdp>0.0) 
					s->eltp += stdp*(3.0 - (s->eltp + s->weight));
				else
					s->eltp += stdp*((s->eltp + s->weight) - (0.1));
			}

			if (src->nid == 201) 
				fprintf(spfile, "%f ", s->eltp);
#endif

			s->calcium -= s->calcium / param_tau_calcium_syn;

			if (t > 300)
			{
				dw = caDP(
			}
		}


		for (nrn_iter ni = this->neurons.begin(); ni != this->neurons.end(); ++ni)
		{

			LANeuron* n= *ni;

			if (n->type == 'S')
			{
				LAInput* in = (LAInput*)n;
				if (t >= in->nextSpikeT && in->nextSpikeT >0)
				{
					if (in->curSpike < in->totalSpikes)
						in->nextSpikeT = in->spikeTimes[in->curSpike++];
					else 
						in->nextSpikeT = -1;
					spikeState[in->nid] = 1;
					lastSpikeT[in->nid] = t;
					in->V = -50;
					//if (in->nid == 400) printf ("T=%d, next=%d\n", t, in->nextSpikeT);
				}
				else
				{
					spikeState[in->nid] = 0;
					in->V = param_E_L;
				}
			}
			else
			{
				if (spikeState[n->nid])
				{
					n->wadapt += param_beta;
					spikeState[n->nid] =0;
					n->V = param_E_L;
					n->actvar +=  (1.0 - n->actvar)/param_tau_actvar;
				}

				double sv = 0;
				for (branch_iter bi = n->branches.begin(); bi != n->branches.end(); ++bi)
				{
					LABranch* b = *bi;
					sv += branch_nonlinearity(b->V);
					b->V -= b->V/param_tau_branch;
				}

				float ww = ((param_g_L * (param_E_L - n->V))+(param_g_L*param_delta_t*exp((n->V-param_thresh)/param_delta_t)) - n->wadapt + sv*6.0 )/param_C ;
				//if (n->nid == 2) printf("SV=%f ww=%f\n", sv, ww);


				n->V += ww;
				n->wadapt += (param_alpha*(n->V - param_E_L)  - n->wadapt)/(n->type == 'I' ? param_tau_wadapt_in : param_tau_wadapt_pyr);


				if (n->V > param_thresh)
				{
					spikeState[n->nid] = 1;
					totalSpikes[n->nid] ++;
					lastSpikeT[n->nid] = t;
					n->V = 0.0;
				}
				else
				{
					spikeState[n->nid] = 0;
				}

			}

			fprintf(outfile, " %f ", n->V);
#if CIMG
			vimg.atXY(t, n->nid) =  n->V;
#endif
	
		}

		fprintf(outfile, "\n");
		fprintf(spfile, "\n");

	}


	int active =0;
	


	for (nrn_iter ni = this->neurons.begin(); ni != this->neurons.end(); ++ni)
	{
		LANeuron* n = *ni;

		float ltp =0.0, oltp =0.0;
		for (syn_iter si = n->incoming.begin(); si != n->incoming.end(); ++si)
		{
			LASynapse* so = *si; 
			ltp += so->eltp;
		}

		for (syn_iter si = n->outgoing.begin(); si != n->outgoing.end(); ++si)
		{
			LASynapse* so = *si; 
			oltp += so->eltp;
		}


		printf("[%d] Spikes : %d total ltp IN: %f, total ltp OUT: %f \n", (*ni)->nid, totalSpikes[(*ni)->nid], ltp, oltp);

		if (totalSpikes[(*ni)->nid] > 15)
			active++;
	}

	printf("Active nrns: %d (%.2f %%)\n", active, float(active)*100.0 / float(this->pyr_list.size()));

	//gplot << LINES << vsurf;

	fclose(outfile);
	fclose(spfile);

#if CIMG
	//synimg.save("synplot.png");
	CImgDisplay window(vimg);
	while (!window.is_closed()) window.wait(10);
#endif

}


void LANetwork::CreateTags()
{
	for (branch_iter bi = this->branches.begin(); bi != this->branches.end(); ++bi)
	{
		LABranch* b =*bi; 
		//float tltp = 0.0;
		for (syn_iter si=b->synapses.begin(); si != b->synapses.end(); ++si)
		{
			//LASynapse* syn = *si;

		}
	}
}




void LANetwork::Interstim(int duration)
{
	int tstop = T + duration;
	while (T < tstop)
	{
		for (syn_iter si=this->synapses.begin(); si != this->synapses.end(); ++si)
		{
			LASynapse* s= *si;
			if (s->eltp != 0.0)
			{
				s->eltp -= s->eltp / param_tau_eltp;
			}
		}


		for (branch_iter bi = this->branches.begin(); bi != this->branches.end(); ++bi)
		{
			//LABranch* b = *bi;
		}
	}
}






void LANetwork::Stimulate(int nInput, int mode)
{
	printf("Stimulating input #%d (%s)... \n", nInput, (mode ? "CS+US" : "CS"));

	int dur = 3000;
	for (nrn_iter n = this->inputs_cs[nInput].begin(); n != this->inputs_cs[nInput].end(); ++n)
	{
		LAInput* in = (LAInput*)*n;
		in->program(200, dur, 80, 0.99);
	}


	if ( mode ==1)
	{
		for (nrn_iter n = this->inputs_us[nInput].begin(); n != this->inputs_us[nInput].end(); ++n)
		{
			LAInput* in = (LAInput*)*n;
			in->program(200, dur, 120, 0.99);
		}
	}

	this->StimDynamics(dur + 600);
	printf("Done.\n");


}


void LANetwork::Begin()
{
	this->Create();
	this->Stimulate(0,1);
}






