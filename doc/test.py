import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt
from random import random

fig = plt.figure()
ax =  Axes3D(fig)

for i in range(1000):
	x = [random(), random()]
	y = [random(), random()]
	z = [random(), random()]
	ax.plot(x, y, z)
ax.legend()
plt.show()
